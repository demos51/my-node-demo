### Node Express template project
Integrated with GitLab for Jira cloud

# Jira boards
https://pedric.atlassian.net/jira/software/c/projects/MND

# Sonarcloud
https://sonarcloud.io/project/overview?id=demos51_my-node-demo

# References
[[1]] Jira smart commits  
[[2]] Switch between branch and merge request pipelines  

[1]:https://confluence.atlassian.com/fisheye/using-smart-commits-960155400.html#UsingSmartCommits-multiline "Jira smart commits"
[2]:https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines "Switch between branch and merge request pipelines"
